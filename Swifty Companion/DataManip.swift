//
//  DataManip.swift
//  Swifty Companion
//
//  Created by Naledi MATUTOANE on 2019/11/28.
//  Copyright © 2019 Naledi MATUTOANE. All rights reserved.
//

import Foundation

class DataManip {
    var currentUser: [String: Any]? = nil
    var searchedUser: [String: Any]? = nil
    var data: [(String, Int, Int)] = []
    
    init(userType: String, data: [String: Any]) {
        switch userType {
        case "current":
            self.currentUser = data
        case "searched":
            self.searchedUser = data
        default:
            break
        }
    }
    
    func getProjects(userType: String) {
        var user: [String: Any]? = nil
        
        switch userType {
        case "current":
            user = currentUser!
        case "searched":
            user = searchedUser!
        default:
            break
        }
        
        let projects = user!["projects_users"]! as? [[String: Any]]
        
        for object in projects! {
            let project = object["project"] as? [String: Any]
            
            if (project!["parent_id"] as? Int == nil) {
                data.append((project!["name"] as! String, object["validated?"] as? Int ?? 0, object["final_mark"] as? Int ?? 0))
            }
            switch userType {
            case "current":
                Projects.currentUserProjects = data.sorted(by: { $0.0.lowercased() < $1.0.lowercased() })
            case "searched":
                Projects.searchedUserProjects = data.sorted(by: { $0.0.lowercased() < $1.0.lowercased() })
            default:
                break
            }
        }
    }
    
    func getSkills(userType: String) {
        var user: [String: Any]? = nil
        
        switch userType {
        case "current":
            user = currentUser
        case "searched":
            user = searchedUser
        default:
            break
        }
        
        let allSkills = user!["cursus_users"]! as? [[String: Any]]
        
        for object in allSkills! {
            let item = (object["cursus"] as? [String: Any])!
            let skills = (object["skills"] as? [[String: Any]])!
            
            for value in skills {
                let name = value["name"] as! String
                let level = value["level"] as! Double
                let cursusName = item["name"] as! String
                
                switch userType {
                case "current":
                    Skills.currentUserSkills.append((cursusName, [(name, level)]))
                case "searched":
                    Skills.searchedUserSkills.append((cursusName, [(name, level)]))
                default:
                    break
                }
            }
        }
    }
}
