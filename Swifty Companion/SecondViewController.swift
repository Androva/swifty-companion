//
//  SecondViewController.swift
//  Swifty Companion
//
//  Created by Naledi MATUTOANE on 2019/11/11.
//  Copyright © 2019 Naledi MATUTOANE. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    var currentUser: [String: Any]? = nil
    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var location: UILabel!
    var userData: DataManip? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentUser = Info.dataToJSON(data: Info.currentUser!)
        userData = DataManip.init(userType: "current", data: self.currentUser!)
        let campus = currentUser!["campus"]! as! [[String: Any]]
        let location = "\(campus[0]["city"]!), \(campus[0]["country"]!)"
        
        fullName.text = "\(currentUser!["first_name"]! as! String) \(currentUser!["last_name"]! as! String)"
        username.text = (currentUser!["login"]! as! String)
        email.text = (currentUser!["email"]! as! String)
        self.location.text = (location)
        
        let url = URL(string: currentUser!["image_url"]! as! String)
        profilePic.downloadImage(from: url!)
        profilePic.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        profilePic.layer.cornerRadius = 10.0
        profilePic.clipsToBounds = true
        
        let projectQueue = DispatchQueue.init(label: "projects")
        let skillsQueue = DispatchQueue.init(label: "skills")
        projectQueue.async {
            self.userData!.getProjects(userType: "current")
        }
        
        skillsQueue.async {
            self.userData!.getSkills(userType: "current")
        }
    }
}

extension UIImageView {
   func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
      URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
   }
    
   func downloadImage(from url: URL) {
      getData(from: url) {
         data, response, error in
         guard let data = data, error == nil else {
            return
         }
         DispatchQueue.main.async() {
            self.image = UIImage(data: data)
         }
      }
   }
}
