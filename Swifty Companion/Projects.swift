//
//  Projects.swift
//  Swifty Companion
//
//  Created by Naledi MATUTOANE on 2019/11/25.
//  Copyright © 2019 Naledi MATUTOANE. All rights reserved.
//

import Foundation

struct Projects {
    static var currentUserProjects: [(String, Int, Int)] = []
    static var searchedUserProjects: [(String, Int, Int)] = []
    
    static func clearData() {
        currentUserProjects = []
        searchedUserProjects = []
    }
}
