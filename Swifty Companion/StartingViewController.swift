//
//  StartingViewController.swift
//  Swifty Companion
//
//  Created by Naledi MATUTOANE on 2019/11/11.
//  Copyright © 2019 Naledi MATUTOANE. All rights reserved.
//

import UIKit
import WebKit

class StartingViewController: UIViewController, WKNavigationDelegate {
    @IBOutlet weak var webView: WKWebView!
    var request: Request? = nil
    var code: String = ""
    var user: Data? = nil {
        didSet {
            if user != oldValue {
                Info.currentUser = user
                Info.currentLogin = Info.dataToJSON(data: Info.currentUser!)!["login"] as! String
                self.performSegue()
            }
        }
    }
    var token: String = ""
    var vc: [UIViewController]? = nil

     override func viewDidLoad() {
        super.viewDidLoad()
        let originalUrl = NSURL(string: "https://api.intra.42.fr/oauth/authorize?client_id=6f6b0c2b2ee1d9934b6924f503120adaa07ff7b6a78ce6a3226ccd9b58bcec0d&redirect_uri=https%3A%2F%2Fintra.42.fr&response_type=code")
        webView.load(URLRequest(url: originalUrl! as URL))
        webView.navigationDelegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if (Info.token != nil) {
            webView.load(URLRequest(url: NSURL(string: "https://api.intra.42.fr/oauth/authorize?client_id=6f6b0c2b2ee1d9934b6924f503120adaa07ff7b6a78ce6a3226ccd9b58bcec0d&redirect_uri=https%3A%2F%2Fintra.42.fr&response_type=code")! as URL))
            webView.navigationDelegate = self
            
//            Info.setView(view: self)
            
//            vc = self.navigationController?.viewControllers
//            print(vc!.count)
//            print(vc![vc!.count - 1].debugDescription)
//            if vc!.count > 0 && (vc![vc!.count - 1]) is StartingViewController {
//                Info.clearData()
//                Projects.clearData()
//                Skills.clearData()
//            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.destination is TabBarController) {
            _ = segue.destination as? TabBarController
        }
    }

    func performSegue() {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "mainApp", sender: self)
        }
    }

    func getQueryStringParameter(queryUrl: String, param: String) -> String? {
            if let newUrl = URLComponents(string: queryUrl) {
                return newUrl.queryItems?.first(where: { $0.name == param })?.value
            }
            else {
                return (nil)
            }
        }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let request = Request.init(view: self)

        if let navigateUrl: URL = navigationAction.request.url {
            if getQueryStringParameter(queryUrl: navigateUrl.absoluteString, param: "code") != nil {
                self.code = getQueryStringParameter(queryUrl: navigateUrl.absoluteString, param: "code")!
                request.createNewSession(code: self.code)
                decisionHandler(.allow)
                return
            }
        }
        decisionHandler(.allow)
    }
}
