//
//  Skills.swift
//  Swifty Companion
//
//  Created by Naledi MATUTOANE on 2019/11/28.
//  Copyright © 2019 Naledi MATUTOANE. All rights reserved.
//

import Foundation

struct Skills {
    static var currentUserSkills: [(String, [(String, Double)])] = []
    static var searchedUserSkills: [(String, [(String, Double)])] = []
    
    static func clearData() {
        currentUserSkills = []
        searchedUserSkills = []
    }
}
