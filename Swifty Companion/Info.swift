//
//  Info.swift
//  Swifty Companion
//
//  Created by Naledi MATUTOANE on 2019/11/11.
//  Copyright © 2019 Naledi MATUTOANE. All rights reserved.
//

import Foundation
import WebKit

struct Info {
    static var startView: StartingViewController? = nil
    static var token: String? = nil
    static var currentUser: Data? = nil
    static var searchedUser: Data? = nil
    static var login: String? = nil
    static var currentLogin: String? = nil
    
    static func dataToJSON(data: Data) -> [String: Any]? {
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
            return (json!)
        }
        catch {
            print(error)
            return (nil)
        }
    }
    
//    static func setView(view: StartingViewController) {
//        self.startView = view
//    }
//
//    static func checkToken() {
//        startView?.performSegue()
//    }
    
    static func clearData() {
        token = nil
        currentUser = nil
        searchedUser = nil
        login = nil
        currentLogin = nil
    }
}
