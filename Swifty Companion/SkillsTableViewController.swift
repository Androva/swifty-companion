//
//  SkillsTableViewController.swift
//  Swifty Companion
//
//  Created by Naledi MATUTOANE on 2019/11/29.
//  Copyright © 2019 Naledi MATUTOANE. All rights reserved.
//

import UIKit

class SkillsTableViewController: UITableViewController {
    var vc: [UIViewController]? = nil
    var previousVC: Any? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vc = self.navigationController?.viewControllers
        previousVC = vc![vc!.count - 2]
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch previousVC {
        case is TabBarController:
            return Skills.currentUserSkills.count
        case is DisplayProfileViewController:
            return Skills.searchedUserSkills.count
        default:
            return 0
        }    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "skillCell", for: indexPath) as! SkillsTableViewCell

        switch previousVC {
        case is TabBarController:
            cell.skill = Skills.currentUserSkills[indexPath.row]
        case is DisplayProfileViewController:
            cell.skill  = Skills.searchedUserSkills[indexPath.row]
        default:
            break
        }
        
        return cell
    }
}
