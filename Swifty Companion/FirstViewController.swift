//
//  FirstViewController.swift
//  Swifty Companion
//
//  Created by Naledi MATUTOANE on 2019/11/11.
//  Copyright © 2019 Naledi MATUTOANE. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    struct User: Codable {
        var id: Int
        var login: String
        var url: String
    }
    
    var searchedData: Data? = nil {
        didSet {
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode([User].self, from: searchedData!)
                
                
                if (model.count < 1) {
                    DispatchQueue.main.async {
                        self.error.text = "Username does not exist"
                        self.error.textColor = UIColor.systemRed
                        self.search.isEnabled = true
                    }
                }
                else {
                    searchedId = model[0].id
                }
            } catch let parsingError {
                if (searchedId != 0) {
                    searchedUser = Info.dataToJSON(data: searchedData!)
                }
                else {
                    print("Error: \(parsingError)")
                }
            }
        }
    }
    var searchedId: Int = 0 {
        didSet {
            if (searchedId != oldValue) {
                request!.getUser(id: searchedId)
            }
        }
    }
    var searchedUser: [String: Any]? = nil {
        didSet {
            DispatchQueue.main.async {
                self.performSegue()
            }
        }
    }
    var request: Request? = nil
    var currentUser: String = Info.dataToJSON(data: Info.currentUser!)!["login"] as! String

    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var search: UIButton!
    @IBOutlet weak var error: UILabel!
    @IBOutlet weak var searchText: UITextField!
    @IBAction func searchButton(_ sender: Any) {
        if searchText.text != "" && searchText.text != nil {
            if searchText.text == Info.currentLogin {
                error.text = "Please head to the profile page to view your profile"
                error.textColor = UIColor.systemRed
            }
            else if searchText.text == Info.login {
                DispatchQueue.main.async {
                    self.performSegue()
                }
            }
            else {
                search.isEnabled = false
                spinner.isHidden = false
                request!.getUserId(username: searchText.text!)
                error.text = ""
            }
        }
        else {
            error.text = "Please insert a username"
            error.textColor = UIColor.systemRed
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    override func viewDidAppear(_ animated: Bool) {
        search.isEnabled = true
        spinner.isHidden = true;
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        request = Request.init(view: self)
                
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        spinner.isHidden = true;
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.destination is DisplayProfileViewController) {
            _ = segue.destination as? DisplayProfileViewController
        }
    }
    
    func performSegue() {
        self.performSegue(withIdentifier: "showProfile", sender: self)
    }
}
