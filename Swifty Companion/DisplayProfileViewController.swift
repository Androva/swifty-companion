//
//  DisplayProfileViewController.swift
//  Swifty Companion
//
//  Created by Naledi MATUTOANE on 2019/11/20.
//  Copyright © 2019 Naledi MATUTOANE. All rights reserved.
//

import UIKit

class DisplayProfileViewController: UIViewController {
    var user: [String: Any]? = nil
    var request: Request? = nil
    var userData: DataManip? = nil
    var project: String? = nil
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var location: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        request = Request.init(view: self)
        user = Info.dataToJSON(data: Info.searchedUser!)
        Info.login = user!["login"] as! String
        userData = DataManip.init(userType: "searched", data: self.user!)
        let campus = user!["campus"]! as! [[String: Any]]
        let location = "\(campus[0]["city"]!), \(campus[0]["country"]!)"
        
        fullName.text = "\(user!["first_name"]! as! String) \(user!["last_name"]! as! String)"
        username.text = (user!["login"]! as! String)
        email.text = (user!["email"]! as! String)
        self.location.text = (location)
        
        let url = URL(string: user!["image_url"]! as! String)
        profilePic.downloadImage(from: url!)
        
        profilePic.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        profilePic.layer.cornerRadius = 10.0
        profilePic.clipsToBounds = true
        
        let projectQueue = DispatchQueue.init(label: "projects")
        let skillsQueue = DispatchQueue.init(label: "skills")
        projectQueue.async {
            self.userData!.getProjects(userType: "searched")
        }
        
        skillsQueue.async {
            self.userData!.getSkills(userType: "searched")
        }
    }
}
