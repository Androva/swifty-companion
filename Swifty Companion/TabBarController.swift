//
//  TabBarController.swift
//  Swifty Companion
//
//  Created by Naledi MATUTOANE on 2019/11/12.
//  Copyright © 2019 Naledi MATUTOANE. All rights reserved.
//

import UIKit
import WebKit

class TabBarController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        HTTPCookieStorage.shared.cookies?.forEach(HTTPCookieStorage.shared.deleteCookie)
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                NSLog("\(cookie)")
            }
        }
    }
}
