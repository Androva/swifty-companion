//
//  SkillsTableViewCell.swift
//  Swifty Companion
//
//  Created by Naledi MATUTOANE on 2019/11/29.
//  Copyright © 2019 Naledi MATUTOANE. All rights reserved.
//

import UIKit

class SkillsTableViewCell: UITableViewCell {
    @IBOutlet weak var skillName: UILabel!
    @IBOutlet weak var cursusName: UILabel!
    @IBOutlet weak var level: UILabel!
    
    var skill: (String, [(String, Double)])? {
        didSet {
            if let s = skill {
                cursusName.text = s.0
                for (key, value) in s.1 {
                    skillName.text = key
                    level.text = String(format: "%.2f", value)
                }
            }
        }
    }
}
