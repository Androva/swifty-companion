//
//  ProjectTableViewCell.swift
//  Swifty Companion
//
//  Created by Naledi MATUTOANE on 2019/11/25.
//  Copyright © 2019 Naledi MATUTOANE. All rights reserved.
//

import UIKit

class ProjectTableViewCell: UITableViewCell {
    @IBOutlet weak var projectName: UILabel!
    @IBOutlet weak var mark: UILabel!
    
    var project: (String, Int, Int)? {
        didSet {
            if let p = project {
                projectName.text = p.0
                mark.text = String(p.2)
                if (p.1 == 0) {
                    mark.textColor = UIColor.systemRed
                }
                else {
                    mark.textColor = UIColor.systemGreen
                }
            }
        }
    }
}
