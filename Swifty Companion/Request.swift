//
//  Request.swift
//  Swifty Companion
//
//  Created by Naledi MATUTOANE on 2019/11/11.
//  Copyright © 2019 Naledi MATUTOANE. All rights reserved.
//

import Foundation
import UIKit

class Request {
    let id: String
    let secret: String
    let bearer: String
    var firstView: FirstViewController? = nil
    var startView: StartingViewController? = nil
    var displayView: DisplayProfileViewController? = nil
    var data: Data? = nil {
        didSet {
            if data != oldValue {
                if startView != nil {
                    startView!.user = data
                }
                else if firstView != nil {
                    if (data!.count > 2) {
                        Info.searchedUser = data
                        firstView!.searchedData = data
                    }
                    else {
                        DispatchQueue.main.async {
                            self.firstView!.error.text = "Username does not exist"
                            self.firstView!.error.textColor = UIColor.systemRed
                            self.firstView!.search.isEnabled = true
                        }                    }
                }
                else if displayView != nil {
                    displayView!.project = String(data: data!, encoding: String.Encoding.utf8)
                }
            }
        }
    }
    
    struct Token: Codable {
        var access_token: String
        var token_type: String
        var expires_in: Int
        var refresh_token: String
        var scope: String
        var created_at: Int
    }
    
    init(view: Any?) {
        if view is StartingViewController {
            self.startView = view as? StartingViewController
        }
        else if view is FirstViewController {
            self.firstView = view as? FirstViewController
        }
        else if view is DisplayProfileViewController {
            self.displayView = view as? DisplayProfileViewController
        }
        
        id = ""
        secret = ""
        bearer = ((id + ":" + secret).data(using: String.Encoding.utf8))!.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
    }

    func createRequest(url: String) -> NSMutableURLRequest {
        let url = NSURL(string: url)
        let request = NSMutableURLRequest(url: url! as URL)
        return (request)
    }

    func createNewSession(code: String) {
        let url = NSURL(string: "https://api.intra.42.fr/oauth/token")!
        let request = NSMutableURLRequest(url: url as URL)
        let params =
            "grant_type=authorization_code" + "&" +
            "client_id=" + id + "&" +
            "client_secret=" + secret + "&" +
            "code=" + code + "&" +
            "redirect_uri=https://intra.42.fr"
            
        
        request.httpMethod = "POST"
        request.setValue("Basic " + bearer, forHTTPHeaderField: "Authorization")
        request.setValue("application/x-www-form-urlencoded;charset-UTF-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = params.data(using: String.Encoding.utf8)

        let task = URLSession.shared.dataTask(with: request as URLRequest,
                                              completionHandler: {
                                                (data, response, error) in
                                                if let d = data {
                                                    Info.token = self.getToken(data: d)
                                                    self.getCurrentUser()
                                                }
                                                else {
                                                    print("No response")
                                                }
        })
        task.resume()
    }
    
        func getToken(data: Data?) -> String? {
            let decoder = JSONDecoder()
            do {
                let json = try decoder.decode(Token.self, from: data!)
                return (json.access_token)
            }
            catch {
                print ("\(error)")
            }
            return (nil)
        }
    
    func makeRequest(method: String, url: URL, params: String?) {
        var request = URLRequest(url: url)
        
        request.httpMethod = method
        request.setValue("Bearer \(Info.token!)", forHTTPHeaderField: "Authorization")
        request.setValue("application/x-www-form-urlencoded;charset-UTF-8", forHTTPHeaderField: "Content-Type")

        if (params != "" && params != nil) {
            request.httpBody = params!.data(using: String.Encoding.utf8)
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest,
                                              completionHandler: {
                                                (data, response, error) in
                                                if let d = data {
                                                self.data = d
                                                }
                                                else {
                                                    print("No response")
                                                }
        })
        task.resume()
    }
    
    func getCurrentUser() {
        self.makeRequest(method: "GET", url: URL(string: "https://api.intra.42.fr/v2/me".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!, params: "")
    }
    
    func getUserId(username: String) {
        self.makeRequest(method: "GET", url: URL(string: "https://api.intra.42.fr/v2/users?filter[login]=\(username)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!, params: "")
    }
    
    func getUser(id: Int) {
        self.makeRequest(method: "GET", url: URL(string: "https://api.intra.42.fr/v2/users/\(id)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!, params: "")
    }
}
